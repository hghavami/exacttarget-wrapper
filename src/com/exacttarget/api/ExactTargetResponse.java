/*
 * Created on Dec 10, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api;


/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public abstract class ExactTargetResponse {

    private boolean successfulRequest = true;
    private String rawResponse = null;
    private String value = "";
    private long timeOfResponse = 0;
    /**
	 * 
	 */
	public ExactTargetResponse() {
		super();
		this.timeOfResponse = System.currentTimeMillis();
	}

    /**
     * This factory will either populate the passed in success object OR detect that is
     * an error response and return an error response
     * 
     * @param xmlResponse
     * @param successObject
     * @return
     * @throws ExactTargetException
     */
    public static final ExactTargetResponse generateResponse(String xmlResponse, ExactTargetResponse successObject) {
        ExactTargetResponse response = null;
        
        if (successObject == null) {
            ExactTargetErrorResponse eResponse = new ExactTargetErrorResponse();
            eResponse.setRawResponse(xmlResponse);
            eResponse.setException(new ExactTargetException("Faild to pass a success response object to staic factory method ExactTargetResponse::generateResponse(). API request may have succeeded."));
            response = eResponse;
            return response;
        }
        
        try {
            // use DOM to parse the XML response            
            int index1 = xmlResponse.indexOf("<error>");
            int index2 = xmlResponse.indexOf("</error>");
            
            if (index1 > 0 && index2 > 0) {
                // create error response
           	String errorCode = xmlResponse.substring(index1 + 7,index2);
            
                response = new ExactTargetErrorResponse();
                response.setValue(errorCode);
                
            }
            else {
                // successful request
                response = successObject;
            }
            
            response.setRawResponse(xmlResponse);
            response.processRawResponse();
        }
        catch (Exception ex) {
            ExactTargetErrorResponse eResponse = new ExactTargetErrorResponse();
            eResponse.setRawResponse(xmlResponse);
            eResponse.setException(ex);
            response = eResponse;           
        }
         
        return response;  
    }
    
    public static final ExactTargetErrorResponse generateExceptionResponse(Exception exp) {
        ExactTargetErrorResponse eResponse = new ExactTargetErrorResponse();
        eResponse.setException(exp);
        return eResponse;           
    }
    
	/**
	 * @return
	 */
	public String getRawResponse() {
		return rawResponse;
	}

	/**
	 * @return
	 */
	public boolean isSuccessfulRequest() {
		return successfulRequest;
	}

	/**
	 * @param string
	 */
	public void setRawResponse(String string) {
		rawResponse = string;
	}

	/**
	 * @param b
	 */
	public void setSuccessfulRequest(boolean b) {
		successfulRequest = b;
	}
	/**
	 * @param b
	 */
	public void setValue(String string) {
		value = string;
	}
	/**
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @return
	 */
	public long getTimeOfResponse() {
		return timeOfResponse;
	}

	public abstract void processRawResponse() throws Exception;
}
