/*
 * Created on Nov 8, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.exacttarget.api.listmanagement;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 * @author swong
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ExactTargetList {
	
	
	private String listName = null;
    private String listType = null;
    
    private HashMap attributes = new HashMap();
    
  
    /**
	 * @return
	 */
	public String getListName() {
		return listName;
	}
	/**
	 * @return
	 */
	public String getListType() {
		return listType;
	}

	/**
	 * 
	 */
	public ExactTargetList() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param string
	 */
	public void setListType(String string) {
		listType = string;
	}
	
	/**
	 * @param string
	 */
	public void setListName(String string) {
		listName = string;
	}
	  /**
     * 
     * @param attributeName
     * @return
     */
    public String getAttribute(String attributeName) {
        return (String)this.attributes.get(attributeName);
    }
    
    /**
     * 
     * @param attributeName
     * @param value
     */
    public void setAttribute(String attributeName, String value) {
        this.attributes.put(attributeName, value);
       
    }
    
    /**
     * 
     * @return Set of attribute names (excluding those required by ET)
     */
    public Set getAttributeNames() {
        return this.attributes.keySet();
    }
    
    /**
     * 
     * @return The collection of non-standard attributes
     */
    public Collection getAttributeValues() {
        return this.attributes.values();
    }
}
