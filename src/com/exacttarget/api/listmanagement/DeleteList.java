/*
 * Created on Jan 10, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.exacttarget.api.listmanagement;

import com.exacttarget.api.ExactTargetResponse;

/**
 * @author swong
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class DeleteList extends ExactTargetListManagementRequest{

	   private String listID = "";
	   private String listType = "";
	   private static final String searchType = "listid";
			

	
	/**
	 * 
	 */
	public DeleteList() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ListManagementRequest#getSearchPart()
	 */
	protected final String getSearchPart() {
        StringBuffer sBuf = new StringBuffer();
        sBuf.append("<search_type>").append(DeleteList.searchType).append("</search_type>");
        sBuf.append("<search_value>").append(this.getListID()).append("</search_value>");

         
		return sBuf.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ListManagementRequest#getActionPart()
	 */
	protected final String getActionPart() {
		return "<action>delete</action>";
	}
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.SubscriberManagementRequest#getValuePart()
	 */
	protected final String getRequiredValuePart() {
        StringBuffer sBuf = new StringBuffer();
       //sBuf.append("<values>");
        // exact target required values
       // sBuf.append("<list_type>").append(this.getListType()).append("</list_type>");
         
        // append the customer specific attributes
        sBuf.append(this.getOptionalValues());
                 
        //sBuf.append("</values>");
		
		return sBuf.toString();
	}
	/* (non-Javadoc)
	 * @see com.exacttarget.api.ExactTargetBaseRequest#parseResponse(java.lang.String)
	 */
	protected ExactTargetResponse parseResponse(String rawResponse) {
        ExactTargetResponse response = null;
        ExactTargetDeleteListManagementResponse successResponse = new ExactTargetDeleteListManagementResponse();
        
        response = ExactTargetResponse.generateResponse(rawResponse, successResponse);
                 
        return response;
	}
	/**
	 * @return
	 */
	public String getListType() {
		return listType;
	}
	/**
     * This method is provided so that subclasses can override it and provide
     * attributes specific to the needs of the customer
     * 
     * @return
     */
    public String getOptionalValues() {
        return ""; 
    }
	/**
	 * @return
	 */
	public String getListID() {
		return listID;
	}
	/**
	 * @param string
	 */
	public void setListType(String string) {
		listType = string;
	}
	/**
	 * @param string
	 */
	public void setListID(String string) {
		listID = string;
	}

}
