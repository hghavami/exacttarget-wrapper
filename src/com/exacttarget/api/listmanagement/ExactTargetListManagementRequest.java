/*
 * Created on Nov 8, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.exacttarget.api.listmanagement;

import com.exacttarget.api.ExactTargetBaseRequest;

/**
 * @author swong
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public abstract class ExactTargetListManagementRequest extends ExactTargetBaseRequest {
    

	/**
	 * 
	 */
	public ExactTargetListManagementRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	 protected abstract String getActionPart();
	 protected abstract String getSearchPart();
	 protected abstract String getRequiredValuePart();
	    
	    
	   
	    
	    /* (non-Javadoc)
	     * @see com.usatoday.exacttarget.api.ExactTargetBaseRequest#getSystemSpecificPart()
	     */
	    protected final String getSystemSpecificPart() {
	        StringBuffer sBuf = new StringBuffer();
	        
	        sBuf.append("<system>");
	        sBuf.append("<system_name>list</system_name>");
	        sBuf.append(this.getActionPart());
	        sBuf.append(this.getSearchPart());
	        sBuf.append(this.getRequiredValuePart());
	        sBuf.append("</system>");
	        
	        return sBuf.toString();
	    }
	    
}
