/*
 * Created on Nov 21, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.exacttarget.api.listmanagement;

import com.exacttarget.api.ExactTargetResponse;

/**
 * @author swong
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ImportStatus  extends ExactTargetListManagementRequest {

	    private String importID = "";



	/**
	 * 
	 */
	public ImportStatus() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ListManagementRequest#getSearchPart()
	 */
	protected final String getSearchPart() {
        StringBuffer sBuf = new StringBuffer();
        sBuf.append("<search_type></search_type>");
        sBuf.append("<search_value>").append(this.getImportID()).append("</search_value>");
        
		return sBuf.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ListManagementRequest#getActionPart()
	 */
	protected final String getActionPart() {
		StringBuffer sBuf = new StringBuffer();
        sBuf.append("<action>import</action>");
        sBuf.append("<sub_action>importstatus</sub_action>");
        
		return sBuf.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.SubscriberManagementRequest#getValuePart()
	 */
	protected final String getRequiredValuePart() {
        StringBuffer sBuf = new StringBuffer();
     
        sBuf.append(this.getOptionalValues());
                 
       sBuf.append("");
		
		return sBuf.toString();
	}
	/* (non-Javadoc)
	 * @see com.exacttarget.api.ExactTargetBaseRequest#parseResponse(java.lang.String)
	 */
	protected ExactTargetResponse parseResponse(String rawResponse) {
        ExactTargetResponse response = null;
        ExactTargetImportStatusManagementResponse successResponse = new ExactTargetImportStatusManagementResponse();
        
        response = ExactTargetResponse.generateResponse(rawResponse, successResponse);
                 
        return response;
	}
	
	/**
     * This method is provided so that subclasses can override it and provide
     * attributes specific to the needs of the customer
     * 
     * @return
     */
    public String getOptionalValues() {
        return ""; 
    }
	
	/**
	
	/**
	 * @param string
	 */
	public void setImportID(String string) {
		importID = string;
	}
	/**
	 * @return
	 */
	public String getImportID() {
		return importID;
	}

}
