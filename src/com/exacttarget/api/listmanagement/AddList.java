/*
 * Created on Nov 1, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.exacttarget.api.listmanagement;

import com.exacttarget.api.ExactTargetResponse;
import com.exacttarget.api.listmanagement.ExactTargetListManagementRequest;
import com.exacttarget.api.listmanagement.ExactTargetListManagementResponse;

/**
 * @author swong
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AddList extends ExactTargetListManagementRequest {

	
	   private String listName = "";
	   private String listType = "";


	/**
	 * 
	 */
	public AddList() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ListManagementRequest#getSearchPart()
	 */
	protected final String getSearchPart() {
        StringBuffer sBuf = new StringBuffer();
        sBuf.append("<search_type></search_type>");
        sBuf.append("<search_value></search_value>");
        
		return sBuf.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ListManagementRequest#getActionPart()
	 */
	protected final String getActionPart() {
		return "<action>add</action>";
	}
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.SubscriberManagementRequest#getValuePart()
	 */
	protected final String getRequiredValuePart() {
        StringBuffer sBuf = new StringBuffer();
       //sBuf.append("<values>");
        // exact target required values
        sBuf.append("<list_name>").append(this.getListName()).append("</list_name>");
        sBuf.append("<list_type>").append(this.getListType()).append("</list_type>");
         
        // append the customer specific attributes
        sBuf.append(this.getOptionalValues());
                 
        //sBuf.append("</values>");
		
		return sBuf.toString();
	}
	/* (non-Javadoc)
	 * @see com.exacttarget.api.ExactTargetBaseRequest#parseResponse(java.lang.String)
	 */
	protected ExactTargetResponse parseResponse(String rawResponse) {
        ExactTargetResponse response = null;
        ExactTargetListManagementResponse successResponse = new ExactTargetListManagementResponse();
        
        response = ExactTargetResponse.generateResponse(rawResponse, successResponse);
                 
        return response;
	}
	/**
	 * @return
	 */
	public String getListType() {
		return listType;
	}
	/**
     * This method is provided so that subclasses can override it and provide
     * attributes specific to the needs of the customer
     * 
     * @return
     */
    public String getOptionalValues() {
        return ""; 
    }
	/**
	 * @return
	 */
	public String getListName() {
		return listName;
	}
	/**
	 * @param string
	 */
	public void setListType(String string) {
		listType = string;
	}
	/**
	 * @param string
	 */
	public void setListName(String string) {
		listName = string;
	}
}
