/*
 * Created on Nov 10, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.exacttarget.api.listmanagement;

import java.util.HashMap;


import com.exacttarget.api.ExactTargetResponse;


/**
 * @author swong
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ImportList  extends ExactTargetListManagementRequest{
	
	   private static final String searchType = "listid";
	   private String searchValue = "";
	   //private String listType = "";
	   private String fileName = "";
	   private String emailAddress = "";
	   private String fileType = "";
	   private String columnHeadings= "";
	   private String listName = "";
	   private String updateAdd = "1";
	   private String returnID = "true";
	   private String encrypted = "false";
	   private String encryptFormat = "";
	   
	   
	   private HashMap field = new HashMap();
		
		
	   String[]fieldarray = new String [] {"Email Address", "PUB", "Account Number","First Name","Last Name",
  		"Company", "Address 1","Address 2","Street","City","Zip","Home Phone","GP Full Name","link",
		"Expire Date", "Start Date", "Term", "Amount", "CC Last 4","CC Type", "Delivery Method",
		"Due Date", "Start Type"};
	
		
		/*	
	   String[]fieldarray = new String [] {"Email Address","ChannelMemberID", "PUB", "Account Number","First Name","Last Name",
  		"Company", "Address 1","Address 2","Street","City","Zip","Home Phone","GP Full Name","link",
		"Expire Date", "Start Date", "Term", "Amount", "CC Last 4","CC Type", "Delivery Method",
		"Due Date", "Start Type"};
	
	   */
	   
	   
	/**
	 * 
	 */
	public ImportList() {
		super();
		// TODO Auto-generated constructor stub
	}
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ListManagementRequest#getSearchPart()
	 */
	protected final String getSearchPart() {
        StringBuffer sBuf = new StringBuffer();
        sBuf.append("<search_type>").append(ImportList.searchType).append("</search_type>");
        sBuf.append("<search_value>").append(this.getSearchValue()).append("</search_value>");
        
        
		return sBuf.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ListManagementRequest#getActionPart()
	 */
	protected final String getActionPart() {
		return "<action>import</action>";
	}
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.SubscriberManagementRequest#getValuePart()
	 */
	protected final String getRequiredValuePart() {
        StringBuffer sBuf = new StringBuffer();
        // exact target required values
        
        sBuf.append("<file_name>").append(this.getFileName()).append("</file_name>");
        sBuf.append("<email_address>").append(this.getEmailAddress()).append("</email_address>");
        sBuf.append("<file_type>").append(this.getFileType()).append("</file_type>");
        sBuf.append("<column_headings>").append(this.getColumnHeadings()).append("</column_headings>");
  
        sBuf.append(this.getFileMapping());
        
        sBuf.append("<update_add>").append(this.getUpdateAdd()).append("</update_add>");
        sBuf.append("<returnid>").append(this.getReturnID()).append("</returnid>");
        sBuf.append("<encrypted>").append(this.getEncrypted()).append("</encrypted>");
        sBuf.append("<encrypt_format>").append(this.getEncryptFormat()).append("</encrypt_format>");
  
        sBuf.append(this.getOptionalValues());
                 
    	
		return sBuf.toString();
	}
	/* (non-Javadoc)
	 * @see com.exacttarget.api.ExactTargetBaseRequest#parseResponse(java.lang.String)
	 */
	protected ExactTargetResponse parseResponse(String rawResponse) {
        ExactTargetResponse response = null;
      
        ExactTargetImportListManagementResponse successResponse = new ExactTargetImportListManagementResponse();
        
        response = ExactTargetResponse.generateResponse(rawResponse, successResponse);
                 
        return response;
	}
	/**
     * This method is provided so that subclasses can override it and provide
     * attributes specific to the needs of the customer
     * 
     * @return
     */
    public String getFileMapping() {
       StringBuffer sBuf = new StringBuffer();
       sBuf.append("<file_mapping>");
         // exact target required values
       
       for(int i=0; i<fieldarray.length; i++){   
       	sBuf.append("<field>").append(fieldarray[i]).append("</field>");
       };             
       
       sBuf.append("</file_mapping>");
 		
 	   return sBuf.toString(); 
    }
	/**
     * This method is provided so that subclasses can override it and provide
     * attributes specific to the needs of the customer
     * 
     * @return
     */
    public String getOptionalValues() {
        return ""; 
    }
	/**
	 * @return
	 */
	public String getListName() {
		return listName;
	}
	
	/**
	 * @return
	 */
	public String getColumnHeadings() {
		return fileType;
	}
	/**
	 * @param string
	 */
	public void setColumnHeadings(String string) {
		fileType = string;
	}
	/**
	 * @return
	 */
	public String getFileType() {
		return fileType;
	}
	/**
	 * @param string
	 */
	public void setFileType(String string) {
		fileType = string;
	}
	/**
	 * @return
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param string
	 */
	public void setFileName(String string) {
		fileName = string;
	}
	/**
	 * @return
	 */
	public String getEmailAddress() {
		return emailAddress;
	}
	/**
	 * @param string
	 */
	public void setEmailAddress(String string) {
		emailAddress = string;
	}
	
	/**
	 * @param string
	 */
	public void setListName(String string) {
		listName = string;
	}
	
	/**
	 * @return
	 */
	public String getUpdateAdd() {
		return updateAdd;
	}
	/**
	 * @param string
	 */
	public void setUpdateAdd(String string) {
		updateAdd = string;
	}
	
	/**
	 * @return
	 */
	public String getReturnID() {
		return returnID;
	}
	/**
	 * @param string
	 */
	public void setReturnID(String string) {
		returnID = string;
	}
	/**
	 * @return
	 */
	public String getEncrypted() {
		return encrypted;
	}
	/**
	 * @param string
	 */
	public void setEncrypted(String string) {
		encrypted = string;
	}
	/**
	 * @return
	 */
	public String getEncryptFormat() {
		return encryptFormat;
	}
	/**
	 * @param string
	 */
	public void setEncryptFormat(String string) {
		encryptFormat = string;
	}
	/**
	 * @return
	 */
	public String getSearchValue() {
		return searchValue;
	}
	/**
	 * @param string
	 */
	public void setSearchValue(String string) {
		searchValue = string;
	}
	
}
