/*
 * Created on Nov 8, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.exacttarget.api.listmanagement;

import com.exacttarget.api.ExactTargetResponse;
import com.exacttarget.api.listmanagement.ExactTargetListManagementRequest;
import com.exacttarget.api.listmanagement.ExactTargetListRetrievalResponse;

/**
 * @author swong
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class RetrieveList extends ExactTargetListManagementRequest  {

	/**
	 * 
	 */
	public RetrieveList() {
		super();
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ExactTargetListManagementRequest#getActionPart()
	 */
	protected final String getActionPart() {
		return "<action>retrieve</action>";
	}

	

	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ExactTargetListManagementRequest#getRequiredValuePart()
	 */
	protected final String getRequiredValuePart() {
		return "";
	}

	/* (non-Javadoc)
	 * @see com.exacttarget.api.ExactTargetBaseRequest#parseResponse(java.lang.String)
	 */
	protected ExactTargetResponse parseResponse(String rawResponse) {
        ExactTargetResponse response = null;
        ExactTargetListRetrievalResponse successResponse = new ExactTargetListRetrievalResponse();
        
        response = ExactTargetResponse.generateResponse(rawResponse, successResponse);
                 
        return response;
   	}
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ExactTargetListManagementRequest#getSearchPart()
	 */
	protected final String getSearchPart() {
		StringBuffer sBuf = new StringBuffer();
        
        sBuf.append("<search_type></search_type>");
        sBuf.append("<search_value></search_value>");
        
		return sBuf.toString();
	}

}
