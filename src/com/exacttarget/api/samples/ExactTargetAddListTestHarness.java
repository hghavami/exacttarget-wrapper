/*
 * Created on Dec 13, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api.samples;

import com.exacttarget.api.ExactTargetResponse;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ExactTargetAddListTestHarness {
/*
 * ExactTarget Seed list containing Nora (Subscriber ID = 1041866070) and Andy�s (Subscriber ID = 1041866938) email addresses (List ID = 268316) 
 * A sample HTML paste email for triggered and targeted email send testing (Email ID = 399892) 
 * A sample template-based email (Email ID = 399866) 
 * Basic subscriber attributes (First Name, Last Name, Email Address, Status, Email Type) 
 */


	/**
	 * 
	 */
	public ExactTargetAddListTestHarness() {
	   super();
	}

	public static void main(String[] args) {
        
       TestAddList newList = new TestAddList();
        
        try {
            
//        	 config stuff
//        	TestAddList.setExactTargetBaseURL("https://www.exacttarget.com/api/integrate.asp");
        	TestAddList.setExactTargetBaseURL("https://api.exacttarget.com/integrate.aspx");
        	newList.setExactTargetUserId("swong");
            newList.setExactTargetPwd("et@1792");
            
            newList.setListName("usatodaysubtest");
            newList.setListType("Public");
              
            ExactTargetResponse response = newList.makeExactTargetRequest();
            
            String res = response.getRawResponse();
            if (response.isSuccessfulRequest()) {
                System.out.println("Response: " + res);
            }
            else {
                System.out.println("Failed to add list...use backup plan");
            }
            
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        
       
        
	}
}
