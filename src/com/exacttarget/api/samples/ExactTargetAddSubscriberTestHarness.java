/*
 * Created on Dec 13, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api.samples;

import com.exacttarget.api.ExactTargetResponse;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ExactTargetAddSubscriberTestHarness {
/*
 * ExactTarget Seed list containing Nora (Subscriber ID = 1041866070) and Andy�s (Subscriber ID = 1041866938) email addresses (List ID = 268316) 
 * A sample HTML paste email for triggered and targeted email send testing (Email ID = 399892) 
 * A sample template-based email (Email ID = 399866) 
 * Basic subscriber attributes (First Name, Last Name, Email Address, Status, Email Type) 
 */


	/**
	 * 
	 */
	public ExactTargetAddSubscriberTestHarness() {
	   super();
	}

	public static void main(String[] args) {
        
//        UsatETAddSubscriber newSub = new UsatETAddSubscriber();
        TestAddSubscriber newSub = new TestAddSubscriber();
        
        try {
            
            // config stuff
        	TestAddSubscriber.setExactTargetBaseURL("http://api.sandbox.exacttarget.com/api/integrate.asp");
            newSub.setExactTargetUserId("api_ent_admin1");
            newSub.setExactTargetPwd("$sandbox2");
            // Test list id for
            newSub.setSearchValueListId("298");
           
            // temporary test        
            //First Name, Last Name, Email Address, Status, Email Type
            newSub.setFirstName("EhSam22");
            newSub.setLastName("Right122");
            newSub.setEmail("sam122@company.com");
            newSub.setChannelMemberId("79");
            
            
        //    String rawRequest = newSub.getRawRequest();
          //  System.out.println("RAW Request: " + rawRequest);
            
            ExactTargetResponse response = newSub.makeExactTargetRequest();
            
            String res = response.getRawResponse();
            if (response.isSuccessfulRequest()) {
                System.out.println("Response: " + res);
            }
            else {
                System.out.println("Failed to add subsciber...use backup plan");
            }
            
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        
        /*
        
        newSub.setAccountNum("341234567");
        newSub.setAddress1("17545 Brookville Ct");
        newSub.setCity("Round Hill");
        newSub.setState("VA");
        newSub.setZip("20141");
        
        
        newSub.setFirstName("Andy");
        newSub.setLastName("East");
        newSub.setFullName("Andy East");
        newSub.setEmail("aeast@usatoday.com");
        newSub.setFirmName("Andy Inc.");
        newSub.setCcLast4Digits("1111");
        newSub.setHomePhone("540-338-5280");
        newSub.setWorkPhone("703-854-5407");
        newSub.setNumCopies("1");
        newSub.setPassword("password");
        newSub.setPublication("USA TODAY");
        newSub.setSubtotal("119.00");
        newSub.setTax("0.00");
        newSub.setTotal("119.00");
        */
        
	}
}
