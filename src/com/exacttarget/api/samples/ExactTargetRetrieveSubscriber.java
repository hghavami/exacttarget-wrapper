/*
 * Created on Nov 24, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.exacttarget.api.samples;

import com.exacttarget.api.ExactTargetResponse;

/**
 * @author swong
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ExactTargetRetrieveSubscriber {

	/**
	 * 
	 */
	public ExactTargetRetrieveSubscriber() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {   
		
	TestRetrieveSubscriber subscriber = new TestRetrieveSubscriber();
    
	  
	
    try {
        
//    	 config stuff
//    	TestRetrieveSubscriber.setExactTargetBaseURL("https://www.exacttarget.com/api/integrate.asp");
    	TestRetrieveSubscriber.setExactTargetBaseURL("https://api.exacttarget.com/integrate.aspx");
    	
        subscriber.setExactTargetUserId("USAToday_API_Subs");
        subscriber.setExactTargetPwd("et@1792");        
        subscriber.setSubscriberListID("1303741");
        subscriber.setEmailAddress("filename.csv");
        subscriber.setChannelMemberID("Public@yourdomain.com");
        subscriber.setListID("csv");
       
        ExactTargetResponse response = subscriber.makeExactTargetRequest();
        
        String res = response.getRawResponse();
        if (response.isSuccessfulRequest()) {
            System.out.println("Response: " + res);
        }
        else {
            System.out.println("Failed to retrieve subscriber...use backup plan");
        }
        
    }
    catch (Exception e) {
        System.out.println(e.getMessage());
        e.printStackTrace();
    }
    
   
    
}

}
