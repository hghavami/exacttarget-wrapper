/*
 * Created on Dec 13, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api.samples;

import com.exacttarget.api.ExactTargetResponse;
import com.exacttarget.api.remotetriggers.IndividualEmailSend;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ETSingleSendTestHarness {

	/**
	 * 
	 */
	public ETSingleSendTestHarness() {
		super();
	}
    
    public static void main(String[] args) {
        
       // RetrieveSubscriber subscriberRetrieve = new RetrieveSubscriber();
        
        IndividualEmailSend req = new IndividualEmailSend();
        
        try {
            
            
            //subscriberRetrieve.setExactTargetUserId("Gannett_API_User003");
            //subscriberRetrieve.setExactTargetPwd("welcome@3");
            //subscriberRetrieve.setEmailAddress("aeast@usatoday.com");
            //subscriberRetrieve.setSubscriberListID("268316");
            
            //ExactTargetResponse res1 = subscriberRetrieve.makeExactTargetRequest();
            
           // if (res1.isSuccessfulRequest()) {
           //     String res = res1.getRawResponse();
            //    System.out.println("Response: " + res);
                
            //    String sid = "";

                // get the id and send the mail
           // }
           // else {
                // add the subscriber and send the mail
                
           // }
            
            // config stuff
            req.setExactTargetUserId("Gannett_API_User003");
            req.setExactTargetPwd("welcome@3");
            // set email id
            req.setEmailTemplateID("401403");  // My sample
            //req.setEmailTemplateID("399892"); // ET Sample Paste HTML
    
            // temporary test        
            req.setSubscriberID("1044731052");
            
        //    String rawRequest = newSub.getRawRequest();
          //  System.out.println("RAW Request: " + rawRequest);
            
            ExactTargetResponse response = req.makeExactTargetRequest();
            
            String res = response.getRawResponse();
            if (response.isSuccessfulRequest()) {
                System.out.println("Response: " + res);
                
            }
            else {
                System.out.println("Failed to single send to subsciber...use backup plan");
            }

        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
}
