/*
 * Created on Dec 6, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.exacttarget.api.samples;
import com.exacttarget.api.ExactTargetResponse;

import java.util.Date;
import java.io.BufferedReader;
import java.io.BufferedWriter;
//import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
//import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.io.IOException;

//import javax.servlet.http.HttpSession;
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.net.URLEncoder;

/**
 * @author swong
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ExactTargetIntegrationTestHarness {
	
	public static String base_URL = "D:\\ExactTarget_Integration\\";
	String outfilename = base_URL + "Logs\\XML_Response_" + getDateFormatted() + ".txt"  ;
	public static String updateCustomerFile =  base_URL  + "exactTargetControl.txt" ;


	/**
	 * 
	 */
	public ExactTargetIntegrationTestHarness() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws IOException{
		
		 
		 boolean updFileExists = (new File(updateCustomerFile)).exists();
		 if (updFileExists) {
	        processFile(updateCustomerFile);
		 } else {
		 	  BufferedWriter buf_writer2 = new BufferedWriter(new FileWriter(base_URL + "Logs\\LogFile_For_Updates_Not_Found_"  + getDateFormatted() + ".txt"));
				
		 	
		 } 	
		
	}	
	

    public static String processFile(String processFile)throws IOException {
    	 int count = 0;
    	 int blanks = 0;
    	 BufferedReader in = new BufferedReader(new FileReader(processFile));
    	 
    	 while (true) {
    	 String s = in.readLine();
    	    	
    	   if (s == null) {
    		   break;
    	   }
    	   java.util.StringTokenizer stringTokenizer= new java.util.StringTokenizer(s,"||"); 
    	   int index = 0; 
    	   int numTokens = stringTokenizer.countTokens(); 
    	   
    	   String[] A = new String[numTokens]; 
    	   if(numTokens>0) 
    	    { 
    	       while(stringTokenizer.hasMoreTokens()) 
    	       { 
    	          String token = stringTokenizer.nextToken(); 
    	          A[index++] = token;
    	       } 
    	    } 
    	   
    		String outfilename = base_URL + "Logs\\XML_Response_" + getDateFormatted() + ".txt"  ;
		    FileWriter out = new FileWriter(outfilename, true);

    		BufferedWriter buf_writer = new BufferedWriter(out);

    		 TestCheckAPI newCheck = new TestCheckAPI();
    	        
    	        try {
    	            
//    	        	config stuff
    	       	    TestCheckAPI.setExactTargetBaseURL(removeQuotes(A[0]));
    	            newCheck.setExactTargetUserId(removeQuotes(A[1]));
    	            newCheck.setExactTargetPwd(removeQuotes(A[2]));
    	            
    	          
    	       
    	            ExactTargetResponse response = newCheck.makeExactTargetRequest();
    	            String res = response.getRawResponse();
    	            String apiStatus = response.getValue();
    		      	
    	           if (response.isSuccessfulRequest()) {
    	                System.out.println("Response: " + res);
    	                
    	            
    	                buf_writer.write( getDateTime() + " " ); 			        
    	                buf_writer.write("Intergration Response for check api"); 
    	                buf_writer.write( "   " + res + " " ); 
    	                buf_writer.newLine(); 
    	                buf_writer.close(); 
    	     	       
    	        	    createList(A);
    	                  }
    	            else {
    	                System.out.println("Exact Target API not available... use backup plan");
    	            }
    	            
    	        }
    	        catch (Exception e) {
    	            System.out.println(e.getMessage());
    	            e.printStackTrace();
    	        }
         
    	}
    	 return "";
    }
    /**
     * This method is provided so that subclasses can override it and provide
     * attributes specific to the needs of the customer
     * 
     * @return
     */
    public static String deleteList(String[] A,String listID) throws IOException{
    	String outfilename = base_URL + "Logs\\XML_Response_" + getDateFormatted() + ".txt"  ;
    	FileWriter out = new FileWriter(outfilename, true);

		BufferedWriter buf_writer = new BufferedWriter(out);

        StringBuffer sBuf = new StringBuffer();
        
        TestDeleteList deleteList = new TestDeleteList();
        
       
        try {
        	
            
//        	 config stuff
        	TestDeleteList.setExactTargetBaseURL(removeQuotes(A[0]));
            deleteList.setExactTargetUserId(removeQuotes(A[1]));
            deleteList.setExactTargetPwd(removeQuotes(A[2]));
            deleteList.setListID(listID);
        	         
                
            ExactTargetResponse response = deleteList.makeExactTargetRequest();
            
            String res = response.getRawResponse();
            if (response.isSuccessfulRequest()) {
                System.out.println("Response: " + res);
                
            
            //  importList("1341064");
                buf_writer.write( getDateTime() + " " ); 			        
                buf_writer.write("Intergration Response for Delete List for listID =  " + listID +" File name=  " + removeQuotes(A[3]) + dateStamp() ); 
                buf_writer.write( "   " + res + " " ); 
                buf_writer.newLine(); 
                buf_writer.close(); 
     	       
      	        
                  }
            else {
                System.out.println("Exact Target Delete not successful... use backup plan");
            }
            
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
       	
 		return sBuf.toString();
    }
	

	/**
     * This method is provided so that subclasses can override it and provide
     * attributes specific to the needs of the customer
     * 
     * @return
     */
    public static String createList(String[] A) throws IOException{
    	String outfilename = base_URL + "Logs\\XML_Response_" + getDateFormatted() + ".txt"  ;
 	FileWriter out = new FileWriter(outfilename, true);

		BufferedWriter buf_writer = new BufferedWriter(out);

        StringBuffer sBuf = new StringBuffer();
        
        TestAddList newList = new TestAddList();
        
        try {
            
//        	 config stuff
        	TestAddList.setExactTargetBaseURL(removeQuotes(A[0]));
            newList.setExactTargetUserId(removeQuotes(A[1]));
            newList.setExactTargetPwd(removeQuotes(A[2]));
            newList.setListName(removeQuotes(A[3]) + dateStamp());
            newList.setListType("Public");
            
              
            ExactTargetResponse response = newList.makeExactTargetRequest();
            
            String res = response.getRawResponse();
            buf_writer.write( getDateTime() + " " ); 			        
            buf_writer.write("Intergration Response for create list-  " + removeQuotes(A[3]) + dateStamp()  ); 
            buf_writer.write( "   " + res + " " ); 
            buf_writer.newLine();
            buf_writer.close(); 
  	       
            
            if (response.isSuccessfulRequest()) {
                System.out.println("Response: " + res);
                String listID = response.getValue();
        		
                
                importList(listID,A);
            }
            else {
                System.out.println("Failed to add list...use backup plan");
            }
            
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        
    	return sBuf.toString();
    }
	
    /**
     * This method is provided so that subclasses can override it and provide
     * attributes specific to the needs of the customer
     * 
     * @return
     */
    public static String importList(String listID, String [] A) throws IOException{
    	String outfilename = base_URL + "Logs\\XML_Response_" + getDateFormatted() + ".txt"  ;
 	FileWriter out = new FileWriter(outfilename, true);

		BufferedWriter buf_writer = new BufferedWriter(out);

        StringBuffer sBuf = new StringBuffer();
        
        TestImportList importList = new TestImportList();
        
    	  
    	
        try {
            
//        	 config stuff
        	
         	
        	TestImportList.setExactTargetBaseURL(removeQuotes(A[0]));
        	
            importList.setExactTargetUserId(removeQuotes(A[1]));
            importList.setExactTargetPwd(removeQuotes(A[2]));        
            importList.setSearchValue(listID);
            importList.setFileName(removeQuotes(A[4]) + dateStamp()+".gpg");
            importList.setEmailAddress("swong@usatoday.com");
            importList.setFileType("csv");
            importList.setColumnHeadings("true");
            importList.setUpdateAdd("0");
            importList.setReturnID("true");
            importList.setEncrypted("true");
            importList.setEncryptFormat("");
                      
            ExactTargetResponse response = importList.makeExactTargetRequest();
            
            String res = response.getRawResponse();
            buf_writer.write( getDateTime() + " " ); 			        
            buf_writer.write("Intergration Response for import list-  " + listID + " and file-  " + removeQuotes(A[4]) + dateStamp()+".gpg"); 
            buf_writer.write( "   " + res + " " ); 
            buf_writer.newLine();
            buf_writer.close(); 
  	       
            if (response.isSuccessfulRequest()) {
                       
                System.out.println("Response: " + res);
                
                String importID = response.getValue();
                importStatus(importID,listID,A);  
              
            }
            else {
            	String errorCode = response.getValue();
                deleteList(A,listID);                
                System.out.println("Failed to import file into list. Err Code = " + errorCode + " - deleting ListID = " + listID );
            }
            
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }   
            
      	
 		return sBuf.toString();
    }
	
    /**
     * This method is provided so that subclasses can override it and provide
     * attributes specific to the needs of the customer
     * 
     * @return
     */
    public static String importStatus(String importID, String listID, String [] A) throws IOException{
    	String outfilename = base_URL + "Logs\\XML_Response_" + getDateFormatted() + ".txt"  ;

        StringBuffer sBuf = new StringBuffer();
        
        String completedSend = "False";
        while (completedSend == "False") {
          
        	
        	FileWriter out = new FileWriter(outfilename, true);

    		BufferedWriter buf_writer = new BufferedWriter(out);

        	TestImportStatus newImportStatus = new TestImportStatus();
        
        	try {
            
//        	 config stuff
        	TestImportList.setExactTargetBaseURL(removeQuotes(A[0]));
            newImportStatus.setExactTargetUserId(removeQuotes(A[1]));
            newImportStatus.setExactTargetPwd(removeQuotes(A[2]));
            
            newImportStatus.setImportID(importID);
            
         
            	ExactTargetResponse response = newImportStatus.makeExactTargetRequest();
            
            	String res = response.getRawResponse();
           
            	
            	  buf_writer.write( getDateTime() + " " ); 			        
                  buf_writer.write("Intergration Response for import status-  " + importID); 
                  buf_writer.write( "   " + res + " " ); 
                  buf_writer.newLine();
                  buf_writer.close(); 
	     	       
                  
            	if (response.isSuccessfulRequest()) {
            		System.out.println("Response: " + res);
            		   
                    String sendInfo = response.getValue();
                    
                    if (sendInfo.equals("Complete"))  {
                    	completedSend = "True";
                        
                    } else {
                   		System.out.println("sleeping... ");
                    	Thread.sleep(2000);
                    	completedSend = "False";
                   		System.out.println("awake... checking import status again");                    
                        
                    }         		
            		
            			
            	}
            	else {
            		
            		System.out.println("Failed to Status Check ImportID... sleeping");
            		Thread.sleep(2000);
             		System.out.println("awake... checking import status again");                    
            		completedSend = "False";
            	}
            
           
        	}
        	catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        	}
        	
        	       
        }
            
        
        if (completedSend == "True") {
    		jobSend(listID,A);
            
        }      
       	
 		return sBuf.toString();
    }
	
	
    /**
     * This method is provided so that subclasses can override it and provide
     * attributes specific to the needs of the customer
     * 
     * @return
     */
    public static String jobSend(String listID,String [] A) throws IOException{
    	String outfilename = base_URL + "Logs\\XML_Response_" + getDateFormatted() + ".txt"  ;

    	FileWriter out = new FileWriter(outfilename, true);

		BufferedWriter buf_writer = new BufferedWriter(out);
        StringBuffer sBuf = new StringBuffer();
        
        TestJobSend jobsend = new TestJobSend();
        
    	  
    	
        try {
            
//        	 config stuff
        	TestImportList.setExactTargetBaseURL(removeQuotes(A[0]));
        	
        	jobsend.setExactTargetUserId(removeQuotes(A[1]));
        	jobsend.setExactTargetPwd(removeQuotes(A[2]));        
          	jobsend.setEmailID(removeQuotes(A[5]));        
         	jobsend.setMultiMime("false");        
         	jobsend.setTrackLinks("false");        
         	jobsend.setSendDate("immediate");        
         	jobsend.setListID(listID);        
                          
                      
            ExactTargetResponse response = jobsend.makeExactTargetRequest();
            
            String res = response.getRawResponse();
            
      	    buf_writer.write( getDateTime() + " " ); 			        
            buf_writer.write("Intergration Response for job send- " + "listID = " + listID + " and emailID= " + removeQuotes(A[5])); 
            buf_writer.write( "   " + res + " " ); 
            buf_writer.newLine();
            buf_writer.close(); 
	       
 
            if (response.isSuccessfulRequest()) {
                System.out.println("Response: " + res);
            }
            else {
                System.out.println("Failed to Send Job...use backup plan");
            }
            
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
 		return sBuf.toString();
    }
	
    /**
     * This method is provided so that subclasses can override it and provide
     * attributes specific to the needs of the customer
     * 
     * @return
     */
    public static String  dateStamp() {
        StringBuffer sBuf = new StringBuffer();
        
        // Make a new Date object. It will be initialized to the current time.
        Date now = new Date();    
        SimpleDateFormat format = 
           new SimpleDateFormat("yyyyMMdd");
      
        sBuf.append(format.format(now));
       	
 		return sBuf.toString();
    }
    

    public static String removeQuotes(String s) {  		   
    	int begIndex = s.indexOf('\"');
    	int endIndex = s.lastIndexOf('\"');   
    	String noQuotes = s.substring((begIndex + 1), endIndex);
    	if (noQuotes.length() == 1) {
    		return "";
    	} else {
    		return noQuotes;
    	}
    }		
    public static String getDateFormatted() {		   	     
       SimpleDateFormat bartDateFormat =
       new SimpleDateFormat("EEEE-MMMM-dd-yyyy");	 
       Date date = new Date();	  
       return bartDateFormat.format(date);
    }

    public static Date getDateTime() {
    	Date date = new Date();
    	return date;
    }
	
}
