/*
 * Created on Dec 13, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api.samples;

import com.exacttarget.api.subscribermanagement.AddSubscriber;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TestAddSubscriber extends AddSubscriber {

    //First Name, Last Name, Email Address, Status, Email Type
    private String firstName = "";
    private String lastName = "";
    
	/**
	 * 
	 */
	public TestAddSubscriber() {
		super();
	}

    
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.AddSubscriber#getOptionalValues()
	 */
	public String getOptionalValues() {
        StringBuffer sBuf = new StringBuffer();
        // our required values
        sBuf.append("<First__Name>").append(this.getFirstName()).append("</First__Name>");
        sBuf.append("<Last__Name>").append(this.getLastName()).append("</Last__Name>");
        
		return sBuf.toString();
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param string
	 */
	public void setLastName(String string) {
		lastName = string;
	}

	/**
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param string
	 */
	public void setFirstName(String string) {
		firstName = string;
	}

}
