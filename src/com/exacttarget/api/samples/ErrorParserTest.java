/*
 * Created on Dec 16, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api.samples;

import com.exacttarget.api.ExactTargetResponse;
import com.exacttarget.api.remotetriggers.ExactTargetTriggerResponse;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ErrorParserTest {

	/**
	 * 
	 */
	public ErrorParserTest() {
		super();
	}

	public static void main(String[] args) {
        
        String responseXML = "<?xml version='1.0'?>  <exacttarget>  <system>  <job>  <job_info>Job was successfully created.</job_info>  <job_description>663780</job_description>  </job>  </system>  </exacttarget>  ";
        
        ExactTargetTriggerResponse response = new ExactTargetTriggerResponse();
        
        ExactTargetResponse response2 = ExactTargetResponse.generateResponse(responseXML, response);
        
        System.out.println("Job Info: " + response.getJobInfo());
        System.out.println("Job Description: " + response.getJobDescription());
        
	}
}
