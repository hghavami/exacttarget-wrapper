/*
 * Created on Dec 16, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api.samples;

import com.exacttarget.api.ExactTargetErrorResponse;
import com.exacttarget.api.ExactTargetResponse;
import com.exacttarget.api.remotetriggers.ExactTargetTriggerResponse;
import com.exacttarget.api.remotetriggers.JobCreationTrigger;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class JobCreationTriggerTest {

	/**
	 * 
	 */
	public JobCreationTriggerTest() {
		super();
		
	}

	public static void main(String[] args) {
        
        JobCreationTrigger jct = new JobCreationTrigger();
        
        try {
            jct.setExactTargetPwd("welcome@3");
            jct.setExactTargetUserId("Gannett_API_User003");
            
            // email template to use
            jct.setEmailTemplateID("401403");
            
            jct.setSendImmediate();
            
            jct.setTrackLinks(false);
            jct.setMultiPartMime(false);
            
            // add a list to send to
            jct.addList("268316");
            
            jct.setFromEmail("aeast@usatoday.com");
            jct.setFromName("USA TODAY - Andy East");
            
            ExactTargetResponse response = jct.makeExactTargetRequest();
            
            if (response.isSuccessfulRequest()) {
                ExactTargetTriggerResponse tResponse = (ExactTargetTriggerResponse)response;
                System.out.println("Job Info: " + tResponse.getJobInfo());
                System.out.println("Job Description: " + tResponse.getJobDescription());
                System.out.println("Raw Response:");
                System.out.println(tResponse.getRawResponse());
            }
            else {
                ExactTargetErrorResponse eResponse = (ExactTargetErrorResponse)response;
                System.out.println("Error Code: " + eResponse.getError().getErrorCode());
                System.out.println("Error Detail: " + eResponse.getError().getErrorDescription());
                System.out.println("Raw Response:");
                System.out.println(eResponse.getRawResponse());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
	}
}
