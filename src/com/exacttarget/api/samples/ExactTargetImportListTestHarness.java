/*
 * Created on Nov 17, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.exacttarget.api.samples;

import com.exacttarget.api.ExactTargetResponse;

/**
 * @author swong
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ExactTargetImportListTestHarness {

	/**
	 * 
	 */
	public ExactTargetImportListTestHarness() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {   
		
	TestImportList importList = new TestImportList();
    
	  
	
    try {
        
//    	 config stuff
//    	TestImportList.setExactTargetBaseURL("https://www.exacttarget.com/api/integrate.asp");
    	TestImportList.setExactTargetBaseURL("https://api.exacttarget.com/integrate.aspx");
    	
        importList.setExactTargetUserId("swong");
        importList.setExactTargetPwd("et@1792");        
        importList.setSearchValue("1316811");
        importList.setFileName("usatformat1.csv");
        importList.setEmailAddress("swong@usatoday.com");
        importList.setFileType("csv");
        importList.setColumnHeadings("true");
        importList.setUpdateAdd("0");
        importList.setReturnID("true");
        importList.setEncrypted("false");
        importList.setEncryptFormat("ascii");
                  
        ExactTargetResponse response = importList.makeExactTargetRequest();
        
        String res = response.getRawResponse();
        if (response.isSuccessfulRequest()) {
            System.out.println("Response: " + res);
        }
        else {
            System.out.println("Failed to import file into list...use backup plan");
        }
        
    }
    catch (Exception e) {
        System.out.println(e.getMessage());
        e.printStackTrace();
    }
    
   
    
}
}
