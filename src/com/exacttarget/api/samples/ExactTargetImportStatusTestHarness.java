/*
 * Created on Nov 22, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.exacttarget.api.samples;
import com.exacttarget.api.ExactTargetResponse;

/**
 * @author swong
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ExactTargetImportStatusTestHarness {

	/**
	 * 
	 */
	public ExactTargetImportStatusTestHarness() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		 TestImportStatus newImportStatus = new TestImportStatus();
	        
	        try {
	            
//	        	 config stuff
//	        	TestImportList.setExactTargetBaseURL("https://www.exacttarget.com/api/integrate.asp");
	        	TestImportList.setExactTargetBaseURL("https://api.exacttarget.com/integrate.aspx");	        	
	            newImportStatus.setExactTargetUserId("USAToday_API_Subs");
	            newImportStatus.setExactTargetPwd("et@1792");
	            
	            newImportStatus.setImportID("4374378");
	              
	            ExactTargetResponse response = newImportStatus.makeExactTargetRequest();
	            
	            String res = response.getRawResponse();
	            if (response.isSuccessfulRequest()) {
	                System.out.println("Response: " + res);
	            }
	            else {
	                System.out.println("Failed to Status Check ImportID...use backup plan");
	            }
	            
	        }
	        catch (Exception e) {
	            System.out.println(e.getMessage());
	            e.printStackTrace();
	        }
	        
	       
	        
		}
	}

