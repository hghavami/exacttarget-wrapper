/*
 * Created on Jan 10, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.exacttarget.api.samples;

import com.exacttarget.api.ExactTargetResponse;

/**
 * @author swong
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ExactTargetDeleteListTestHarness {

	/**
	 * 
	 */
	public ExactTargetDeleteListTestHarness() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		 TestDeleteList deleteList = new TestDeleteList();
	        
	        try {
	            
//	        	 config stuff
//	        	TestDeleteList.setExactTargetBaseURL("https://www.exacttarget.com/api/integrate.asp");
	        	TestDeleteList.setExactTargetBaseURL("https://api.exacttarget.com/integrate.aspx");	        	
	            deleteList.setExactTargetUserId("swong");
	            deleteList.setExactTargetPwd("et@1792");
	            deleteList.setListID("1386559");
	        	         
	         //   deleteList.setListName("usatodaysubtest");
	          //  deleteList.setListType("Public");
	              
	            ExactTargetResponse response = deleteList.makeExactTargetRequest();
	            
	            String res = response.getRawResponse();
	            if (response.isSuccessfulRequest()) {
	                System.out.println("Response: " + res);
	            }
	            else {
	                System.out.println("Failed to add list...use backup plan");
	            }
	            
	        }
	        catch (Exception e) {
	            System.out.println(e.getMessage());
	            e.printStackTrace();
	        }
	        
	       
	        
		}
	
}
