/*
 * Created on Nov 30, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.exacttarget.api.jobmanagement;

import com.exacttarget.api.ExactTargetResponse;
import com.exacttarget.api.jobmanagement.ExactTargetJobManagementResponse;

/**
 * @author swong
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class JobSend extends ExactTargetJobManagementRequest {

	   private static final String searchType = "emailid";
	   private String emailID = "";
		 
	   private String listName = "";
	   private String listType = "";
	   private String fromName = "";
	   private String fromEmail = "";
	   private String additional = "";
	   private String multiMime = "";
	   private String trackLinks = "";
	   private String sendDate = "";
	   private String sendTime = "";
	   private String listID = "";

	   
	   
	   
	   
	/**
	 * 
	 */
	public JobSend() {
		super();
		// TODO Auto-generated constructor stub
	}
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ListManagementRequest#getSearchPart()
	 */
	protected final String getSearchPart() {
        StringBuffer sBuf = new StringBuffer();
        sBuf.append("<search_type>").append(this.getSearchType()).append("</search_type>");
        sBuf.append("<search_value>").append(this.getEmailID()).append("</search_value>");
        
		return sBuf.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ListManagementRequest#getActionPart()
	 */
	protected final String getActionPart() {
		return "<action>send</action>";
	}
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.SubscriberManagementRequest#getValuePart()
	 */
	protected final String getRequiredValuePart() {
        StringBuffer sBuf = new StringBuffer();
       //sBuf.append("<values>");
        // exact target required values
              
        // append the customer specific attributes
        sBuf.append(this.getOptionalValues());
                 
        sBuf.append("<multipart_mime>").append(this.getMultiMime()).append("</multipart_mime>");
        sBuf.append("<track_links>").append(this.getTrackLinks()).append("</track_links>");
        sBuf.append("<send_date>").append(this.getSendDate()).append("</send_date>");
        sBuf.append("<send_time>").append(this.getSendTime()).append("</send_time>");
           
        sBuf.append("<lists>");        	
        sBuf.append(this.getListValues());
        sBuf.append("</lists>");
        
  
		return sBuf.toString();
	}
	/* (non-Javadoc)
	 * @see com.exacttarget.api.ExactTargetBaseRequest#parseResponse(java.lang.String)
	 */
	protected ExactTargetResponse parseResponse(String rawResponse) {
        ExactTargetResponse response = null;
        ExactTargetJobManagementResponse successResponse = new ExactTargetJobManagementResponse();
        
        response = ExactTargetResponse.generateResponse(rawResponse, successResponse);
                 
        return response;
	}
	/**
	 * @return
	 */
	public String getListType() {
		return listType;
	}
	/**
     * This method is provided so that subclasses can override it and provide
     * attributes specific to the needs of the customer
     * 
     * @return
     */
    public String getOptionalValues() {
        StringBuffer sBuf = new StringBuffer();
      
        sBuf.append("<from_name>").append(this.getFromName()).append("</from_name>");
        sBuf.append("<from_email>").append(this.getFromEmail()).append("</from_email>");
        sBuf.append("<additional>").append(this.getListType()).append("</additional>");
      	
 		return sBuf.toString();
    }
	
    /**
     * This method is provided so that subclasses can override it and provide
     * attributes specific to the needs of the customer
     * 
     * @return
     */
    public String getListValues() {
        StringBuffer sBuf = new StringBuffer();
      
        sBuf.append("<list>").append(this.getListID()).append("</list>");
      	
 		return sBuf.toString();
    }
	/**
	 * @param string
	 */
	public void setListType(String string) {
		listType = string;
	}
	/**
	 * @return
	 */
	public String getListName() {
		return listName;
	}
	/**
	 * @param string
	 */
	public void setListName(String string) {
		listName = string;
	}
	/**
	 * @return
	 */
	public String getSearchType() {
		return searchType;
	}
	
	
	/**
	 * @return
	 */
	public String getFromName() {
		return fromName;
	}
	
	/**
	 * @param string
	 */
	public void setFromName(String string) {
		fromName = string;
	}
	/**
	 * @return
	 */
	public String getFromEmail() {
		return fromEmail;
	}
	
	/**
	 * @param string
	 */
	public void setFromEmail(String string) {
		fromEmail = string;
	}
	/**
	 * @return
	 */
	public String getEmailID() {
		return emailID;
	}
	
	/**
	 * @param string
	 */
	public void setEmailID(String string) {
		emailID = string;
	}
	/**
	 * @return
	 */
	public String getListID() {
		return listID;
	}
	
	/**
	 * @param string
	 */
	public void setListID(String string) {
		listID = string;
	}
	/**
	 * @return
	 */
	public String getTrackLinks() {
		return trackLinks;
	}
	
	/**
	 * @param string
	 */
	public void setTrackLinks(String string) {
		trackLinks = string;
	}
	
	/**
	 * @return
	 */
	public String getSendDate() {
		return sendDate;
	}
	
	/**
	 * @param string
	 */
	public void setSendDate(String string) {
		sendDate = string;
	}
	

	/**
	 * @return
	 */
	public String getSendTime() {
		return sendTime;
	}
	
	/**
	 * @param string
	 */
	public void setSendTime(String string) {
		sendTime = string;
	}
	
	
	/**
	 * @return
	 */
	public String getMultiMime() {
		return multiMime;
	}
	
	/**
	 * @param string
	 */
	public void setMultiMime(String string) {
		multiMime = string;
	}
	
	
}
