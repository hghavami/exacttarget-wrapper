/*
 * Created on Nov 15, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.exacttarget.api.diagnosticsmanagement;

import com.exacttarget.api.ExactTargetResponse;

/**
 * @author swong
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CheckAPI extends ExactTargetDiagnosticsManagementRequest{

	
	private String actionValue = "";
	   
	/**
	 * 
	 */
	public CheckAPI() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ListManagementRequest#getSearchPart()
	 */
	protected final String getSearchPart() {
        StringBuffer sBuf = new StringBuffer();
       
		return sBuf.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ListManagementRequest#getActionPart()
	 */
	protected final String getActionPart() {
		return "<action>ping</action>";
	}
	
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.DiagnosticsManagementRequest#getValuePart()
	 */
	protected final String getRequiredValuePart() {
        StringBuffer sBuf = new StringBuffer();
        // exact target required values
       
		return sBuf.toString();
	}
	/* (non-Javadoc)
	 * @see com.exacttarget.api.ExactTargetBaseRequest#parseResponse(java.lang.String)
	 */
	protected ExactTargetResponse parseResponse(String rawResponse) {
        ExactTargetResponse response = null;
        ExactTargetDiagnosticsManagementResponse successResponse = new ExactTargetDiagnosticsManagementResponse();
     //   String resStatus = successResponse.getStatus();
        
      response = ExactTargetResponse.generateResponse(rawResponse, successResponse);
     //    response = ExactTargetResponse.generateResponse(resStatus, successResponse);
        String resStatus = successResponse.getStatus();
                      
        return response;
        
	}
	
	/**
     * This method is provided so that subclasses can override it and provide
     * attributes specific to the needs of the customer
     * 
     * @return
     */
    public String getOptionalValues() {
        return ""; 
    }
	
	

}
