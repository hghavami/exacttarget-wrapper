/*
 * Created on Dec 10, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api;



/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ExactTargetError {

    private String errorCode = null;
    private String errorDescription = null;
    private String system = null;
    private long timeOfError = 0;
    
	/**
	 * 
	 */
	public ExactTargetError() {
		super();
        this.timeOfError = System.currentTimeMillis();
	}

	/**
	 * @return
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @return
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * @return
	 */
	public String getSystem() {
		return system;
	}

	/**
	 * @return
	 */
	public long getTimeOfError() {
		return timeOfError;
	}

	/**
	 * @param string
	 */
	public void setErrorCode(String string) {
		errorCode = string;
	}

	/**
	 * @param string
	 */
	public void setErrorDescription(String string) {
		errorDescription = string;
	}

	/**
	 * @param string
	 */
	public void setSystem(String string) {
		system = string;
	}

}
