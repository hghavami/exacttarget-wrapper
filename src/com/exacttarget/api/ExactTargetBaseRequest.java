/*
 * Created on Dec 10, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public abstract class ExactTargetBaseRequest implements ExactTargetAPIIntf{

    public static final String XMLVersionTag = "<?xml version=\"1.0\" ?>";
    
//    private static String exactTargetBaseURL = "https://www.exacttarget.com/api/integrate.asp";
    private static String exactTargetBaseURL = "https://api.exacttarget.com/integrate.aspx";    
 
    private String exactTargetUserId = null;
    private String exactTargetPwd = null;
    
    private String rawRequest = null;
    private ExactTargetResponse response = null;
    
    private Exception requestException = null;
    
	/**
	 * 
	 */
	public ExactTargetBaseRequest() {
		super();
	}


	/**
	 * @return
	 */
	public String getExactTargetPwd() {
		return exactTargetPwd;
	}

	/**
	 * @return
	 */
	public String getExactTargetUserId() {
		return exactTargetUserId;
	}

	/**
	 * @param string
	 */
	public void setExactTargetPwd(String string) {
		exactTargetPwd = string;
	}

	/**
	 * @param string
	 */
	public void setExactTargetUserId(String string) {
		exactTargetUserId = string;
	}

	/**
	 * @return
	 */
	public static String getExactTargetBaseURL() {
		return exactTargetBaseURL;
	}

	/**
	 * @param string
	 */
	public static void setExactTargetBaseURL(String string) {
		exactTargetBaseURL = string;
	}

    private String getAuthorizationPart() {
        StringBuffer sBuf = new StringBuffer();
        
        // build the authorization XML
        // <authorization>
        //      <username>Usat users</username>
        //      <password>Usat password</password>
        // </authorization>
        sBuf.append("<authorization><username>");
        sBuf.append(this.getExactTargetUserId());
        sBuf.append("</username><password>");
        sBuf.append(this.getExactTargetPwd());
        sBuf.append("</password></authorization>");
        
        return sBuf.toString();
    }
    
    /**
     * Subclasses must implement this portion for building the system
     * specific request
     */
    protected abstract String getSystemSpecificPart();
    
    /**
     * Subclasses must provide the implementation for parsing the responses from ET
     * @return
     */
    protected abstract ExactTargetResponse parseResponse(String rawResponse);
    
    /**
     * 
     * @return
     */
    public final String generateRawXML() {
        StringBuffer rawBuf = new StringBuffer();
        
        // append the XML Version
        rawBuf.append(ExactTargetBaseRequest.XMLVersionTag);
        // Start the outter request block
        rawBuf.append("<exacttarget>");
        // add the authorization block
        rawBuf.append(this.getAuthorizationPart());
        
        // call the subclass implementation for specifics of this request
        rawBuf.append(this.getSystemSpecificPart());
        
        // end exacttarget block
        rawBuf.append("</exacttarget>");
        
        this.rawRequest = rawBuf.toString();
        return this.rawRequest;
    }
    
    
	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ExactTargetAPIIntf#makeExactTargetRequest()
	 */
	public final ExactTargetResponse makeExactTargetRequest() {
        
        HttpURLConnection urlConn = null;
        
        try {
    		URL url = new URL(ExactTargetBaseRequest.getExactTargetBaseURL());
            
            urlConn = (HttpURLConnection)url.openConnection();
            
            urlConn.setRequestMethod("POST");
            urlConn.setDoInput(true);
            urlConn.setDoOutput(true);
            
            urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            
            OutputStream os = urlConn.getOutputStream();
                        
            String xml = "qf=xml&xml=" + URLEncoder.encode(this.generateRawXML(), "UTF-8");
            
            os.write(xml.getBytes());
            
            os.close();
            
            // get the ExactTarget response
            
            BufferedInputStream bis =  new BufferedInputStream(urlConn.getInputStream());
            
            StringBuffer rawResponse = new StringBuffer();
            
            byte [] buf = new byte[512];
            int bytesRead = 0;
            while ((bytesRead = bis.read(buf)) > 0){
                String contentEncoding = "utf-8";
                String temp = new String(buf, 0, bytesRead, contentEncoding);
                rawResponse.append(temp);
            }
            bis.close();
            
            // Build the response object
            this.response = this.parseResponse(rawResponse.toString());
            
        }
        catch (MalformedURLException me) {
            this.response = ExactTargetResponse.generateExceptionResponse(me);
        }
        catch (IOException ioe) {
            this.response = ExactTargetResponse.generateExceptionResponse(ioe);
            this.requestException = ioe;            
        }
        finally {
            if (urlConn != null){
                urlConn.disconnect();
            }
        }
        
		return this.response;
	}

	/**
	 * @return
	 */
	public Exception getRequestException() {
		return requestException;
	}

	/**
	 * @return
	 */
	public ExactTargetResponse getResponse() {
		return response;
	}

	/**
	 * @return
	 */
	public String getRawRequest() {
        if (rawRequest == null) {
            return this.generateRawXML();
        }
        else {
            return rawRequest;
        }
	}

}
