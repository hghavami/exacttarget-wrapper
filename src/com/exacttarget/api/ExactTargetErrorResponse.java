/*
 * Created on Dec 16, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api;

import java.io.ByteArrayInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ExactTargetErrorResponse extends ExactTargetResponse {

	private Exception exception = null;
    private ExactTargetError error = null;
    

	/**
	 * 
	 */
	public ExactTargetErrorResponse() {
		super();
        this.setSuccessfulRequest(false);
	}

	/**
     * SAMPLE ERROR FORMAT:
     * <?xml version="1.0" ?>
     * <exacttarget>
     * <system>
     * <subscriber>
     * <error>87</error>
     * <error_description>An application security violation has occurred. You
     * are not authorized to view the requested data. The event has been
     * logged. If you feel you have received this message in error, or would
     * like more information, please contact us.</error_description>
     * </subscriber>
     * </system>
     * </exacttarget> 
	 * @see com.exacttarget.api.ExactTargetResponse#processRawResponse()
	 */
	public void processRawResponse()  throws Exception {
		if (this.getRawResponse() == null || this.getRawResponse().length() == 0) {
            throw new ExactTargetException("Must set the raw response before parsing it!");   
		}
        
        this.error = new ExactTargetError();
        
        // use DOM to parse the XML response
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            
        DocumentBuilder builder = factory.newDocumentBuilder();
            
            
        ByteArrayInputStream bais = new ByteArrayInputStream(this.getRawResponse().getBytes()); 
            
        Document d = builder.parse( bais );    
        
        // process error info
        NodeList nList = d.getElementsByTagName("error");
            
        if (nList.getLength() != 1) {
            this.error.setErrorCode("39");
            this.error.setErrorDescription("Unable to parse ExactTarget Response. Expected <error> tag.");
            return;
        }
    
        Node node = nList.item(0);
        Node childNode = node.getFirstChild();
            
        String temp = childNode.getNodeValue();
        
        error.setErrorCode(temp);
        
        // process error description        
        nList = d.getElementsByTagName("error_description");
            
        if (nList.getLength() != 1) {
            this.error.setErrorDescription("Unable to parse ExactTarget Response. Expected <error_description> tag.");
            return;
        }
    
        node = nList.item(0);
        childNode = node.getFirstChild();
            
        temp = childNode.getNodeValue();
        
        error.setErrorDescription(temp);
        
	}

	/**
	 * @return
	 */
	public Exception getException() {
		return exception;
	}

	/**
	 * @param exception
	 */
	public void setException(Exception exception) {
		this.exception = exception;
	}

    
	/**
	 * @return
	 */
	public ExactTargetError getError() {
		return error;
	}

}
