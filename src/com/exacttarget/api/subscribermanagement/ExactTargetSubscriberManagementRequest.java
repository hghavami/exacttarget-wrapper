/*
 * Created on Dec 10, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api.subscribermanagement;

import com.exacttarget.api.ExactTargetBaseRequest;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public abstract class ExactTargetSubscriberManagementRequest extends ExactTargetBaseRequest {
                
	/**
	 * 
	 */
	public ExactTargetSubscriberManagementRequest() {
		super();
	}
    
    protected abstract String getActionPart();
    protected abstract String getSearchPart();
    protected abstract String getRequiredValuePart();
    
    /* (non-Javadoc)
     * @see com.usatoday.exacttarget.api.ExactTargetBaseRequest#getSystemSpecificPart()
     */
    protected final String getSystemSpecificPart() {
        StringBuffer sBuf = new StringBuffer();
        
        sBuf.append("<system>");
        sBuf.append("<system_name>subscriber</system_name>");
        sBuf.append(this.getActionPart());
        sBuf.append(this.getSearchPart());
        sBuf.append(this.getRequiredValuePart());
        sBuf.append("</system>");
        
        return sBuf.toString();
    }
}
