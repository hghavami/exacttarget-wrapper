/*
 * Created on Dec 10, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api.subscribermanagement;

import com.exacttarget.api.ExactTargetResponse;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AddSubscriber extends ExactTargetSubscriberManagementRequest {

    private static final String searchType = "listid";
    private String searchValueListId = "";
    private String channelMemberId = "";


    // add getter and setters ET required attributes
    private String email = "";

	/**
	 * 
	 */
	public AddSubscriber() {
		super();
	}


	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.SubscriberManagementRequest#getActionPart()
	 */
	protected final String getActionPart() {
		return "<action>add</action>";
	}

	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.SubscriberManagementRequest#getSearchPart()
	 */
	protected final String getSearchPart() {
        StringBuffer sBuf = new StringBuffer();
        sBuf.append("<search_type>").append(AddSubscriber.searchType).append("</search_type>");
        sBuf.append("<search_value>").append(this.getSearchValueListId()).append("</search_value>");
        
		return sBuf.toString();
	}

	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.SubscriberManagementRequest#getValuePart()
	 */
	protected final String getRequiredValuePart() {
	
        StringBuffer sBuf = new StringBuffer();
        sBuf.append("<values>");
        // exact target required values
        sBuf.append("<Email__Address>").append(this.getEmail()).append("</Email__Address>");
        sBuf.append("<ChannelMemberID>").append(this.getChannelMemberId()).append("</ChannelMemberID>");
         
        // append the customer specific attributes
        sBuf.append(this.getOptionalValues());
                 
        sBuf.append("</values>");
		
		return sBuf.toString();
		
	}

    
    /**
     * This method is provided so that subclasses can override it and provide
     * attributes specific to the needs of the customer
     * 
     * @return
     */
    public String getOptionalValues() {
        return ""; 
    }

	/**
	 * @return
	 */
	public String getChannelMemberId() {
		return channelMemberId;
	}
	
	/**
	 * @return
	 */
	public String getSearchValueListId() {
		return searchValueListId;
	}

	/**
	 * @param string
	 */
	public void setSearchValueListId(String string) {
		searchValueListId = string;
	}

	/**
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param string
	 */
	public void setEmail(String string) {
		email = string;
	}
	
	/**
	 * @param string
	 */
	public void setChannelMemberId(String string) {
		channelMemberId = string;
	}
	/* (non-Javadoc)
	 * @see com.exacttarget.api.ExactTargetBaseRequest#parseResponse(java.lang.String)
	 */
	protected ExactTargetResponse parseResponse(String rawResponse) {
        ExactTargetResponse response = null;
        ExactTargetSubscriberManagementResponse successResponse = new ExactTargetSubscriberManagementResponse();
        
        response = ExactTargetResponse.generateResponse(rawResponse, successResponse);
                 
        return response;
	}

}
