/*
 * Created on Dec 13, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api.subscribermanagement;

import com.exacttarget.api.ExactTargetResponse;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class RetrieveSubscriber
	extends ExactTargetSubscriberManagementRequest {
        
    private String subscriberListID = "";
    private String emailAddress = "";
    private String channelMemberID = "";
    private String listID = "";

	/**
	 * 
	 */
	public RetrieveSubscriber() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ExactTargetSubscriberManagementRequest#getActionPart()
	 */
	protected final String getActionPart() {
		return "<action>retrieve</action>";
	}

	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ExactTargetSubscriberManagementRequest#getSearchPart()
	 */
	protected final String getSearchPart() {
		StringBuffer sBuf = new StringBuffer();
        
        sBuf.append("<search_type>").append(this.getListID()).append("</search_type>");
        sBuf.append("<search_value>").append(this.getSubscriberListID()).append("</search_value>");
        sBuf.append("<search_value2>").append(this.getEmailAddress()).append("</search_value2>");
        sBuf.append("<showChannelID>").append(this.getChannelMemberID()).append("</showChannelID>");
        
        
        
		return sBuf.toString();
	}

	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ExactTargetSubscriberManagementRequest#getRequiredValuePart()
	 */
	protected final String getRequiredValuePart() {
		return "";
	}
	/**
	 * @return
	 */
	public String getListID() {
		return listID;
	}

	/**
	 * @param string
	 */
	public void setListID(String string) {
		listID = string;
	}
	
	/**
	 * @return
	 */
	public String getSubscriberListID() {
		return subscriberListID;
	}

	/**
	 * @param string
	 */
	public void setSubscriberListID(String string) {
		subscriberListID = string;
	}
	/**
	 * @return
	 */
	public String getChannelMemberID() {
		return channelMemberID;
	}

	/**
	 * @param string
	 */
	public void setChannelMemberID(String string) {
		channelMemberID = string;
	}
	/**
	 * @return
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param string
	 */
	public void setEmailAddress(String string) {
		emailAddress = string;
	}

	/* (non-Javadoc)
	 * @see com.exacttarget.api.ExactTargetBaseRequest#parseResponse(java.lang.String)
	 */
	protected ExactTargetResponse parseResponse(String rawResponse) {
        ExactTargetResponse response = null;
        ExactTargetSubscriberRetrievalResponse successResponse = new ExactTargetSubscriberRetrievalResponse();
        
        response = ExactTargetResponse.generateResponse(rawResponse, successResponse);
                 
        return response;
   	}

}
