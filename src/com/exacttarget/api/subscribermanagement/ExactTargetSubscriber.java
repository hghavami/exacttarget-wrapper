/*
 * Created on Dec 16, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api.subscribermanagement;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ExactTargetSubscriber {

    private String emailAddress = null;
    private String subscriberID = null;
    private String emailType = null;
    private String status = null;
    
    private HashMap attributes = new HashMap();
    
	/**
	 * 
	 */
	public ExactTargetSubscriber() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @return
	 */
	public String getEmailType() {
		return emailType;
	}

	/**
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	
	/**
	 * @return
	 */
	public String getSubscriberID() {
		return subscriberID;
	}

	/**
	 * @param string
	 */
	public void setEmailAddress(String string) {
		emailAddress = string;
	}
	/**
	 * @param string
	 */
	public void setEmailType(String string) {
		emailType = string;
	}

	/**
	 * @param string
	 */
	public void setStatus(String string) {
		status = string;
	}

	/**
	 * @param string
	 */
	public void setSubscriberID(String string) {
		subscriberID = string;
	}

    /**
     * 
     * @param attributeName
     * @return
     */
    public String getAttribute(String attributeName) {
        return (String)this.attributes.get(attributeName);
    }
    
    /**
     * 
     * @param attributeName
     * @param value
     */
    public void setAttribute(String attributeName, String value) {
        this.attributes.put(attributeName, value);
       
    }
    
    /**
     * 
     * @return Set of attribute names (excluding those required by ET)
     */
    public Set getAttributeNames() {
        return this.attributes.keySet();
    }
    
    /**
     * 
     * @return The collection of non-standard attributes
     */
    public Collection getAttributeValues() {
        return this.attributes.values();
    }
}
