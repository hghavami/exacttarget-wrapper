/*
 * Created on Dec 13, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api.remotetriggers;


/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class IndividualEmailSend extends ExactTargetTrigger {

    private String subscriberID = "";
    
	/**
	 * 
	 */
	public IndividualEmailSend() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ExactTargetJobRequest#getActionPart()
	 */
	protected final String getActionPart() {
		return "<action>send_single</action>";
	}

	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ExactTargetJobRequest#getSearchPart()
	 */
	protected final String getSearchPart() {
		StringBuffer sBuf = new StringBuffer();
        
        // specify that this is the email template search type
        sBuf.append("<search_type>emailid</search_type>");
        sBuf.append("<search_value>").append(this.getEmailTemplateID()).append("</search_value>");
        sBuf.append("<search_value2>").append(this.getSubscriberID()).append("</search_value2>");
        
		return sBuf.toString();
	}

	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ExactTargetJobRequest#getRequiredValuePart()
	 */
	protected final String getRequiredValuePart() {
        return "";
	}

	/**
	 * @return
	 */
	public final String getSubscriberID() {
		return subscriberID;
	}

	/**
	 * @param string
	 */
	public void setSubscriberID(String string) {
		subscriberID = string;
	}

}
