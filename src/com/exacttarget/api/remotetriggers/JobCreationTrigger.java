/*
 * Created on Dec 13, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api.remotetriggers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.exacttarget.api.ExactTargetException;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class JobCreationTrigger extends ExactTargetTrigger {

    private String fromName = "";              // Senders from name
    private String fromEmail = "";             // Senders email address
    private String additional = "";            // If activated, oherwise empty tag
    private boolean multiPartMime = false;     // default to false
    private boolean trackLinks = false;        // default to not track links
    private String sendDate = "immediate";     // default to immediate
    private String sendTime = "";              // send time , hh:mm format 24hr clock
    private ArrayList lists = new ArrayList(); // container for the lists
      
	/**
	 * 
	 */
	public JobCreationTrigger() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ExactTargetJobRequest#getActionPart()
	 */
	protected final String getActionPart() {
		return "<action>send</action>";
	}

	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ExactTargetJobRequest#getSearchPart()
	 */
	protected final String getSearchPart() {
		StringBuffer sBuf = new StringBuffer();
        
        sBuf.append("<search_type>emailid</search_type>");
        sBuf.append("<search_value>").append(this.getEmailTemplateID()).append("</search_value>");
        
		return sBuf.toString();
	}

	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ExactTargetJobRequest#getRequiredValuePart()
	 */
	protected final String getRequiredValuePart() {
        StringBuffer sBuf = new StringBuffer();
        
		sBuf.append("<from_name>").append(this.getFromName()).append("</from_name>");
        sBuf.append("<from_email>").append(this.getFromEmail()).append("</from_email>");
        sBuf.append("<additional>").append(this.getAdditional()).append("</additional>");
        if (this.isMultiPartMime()){
            sBuf.append("<multipart_mime>true</multipart_mime>");
        }
        else {
            sBuf.append("<multipart_mime>false</multipart_mime>");
        }
        if (this.isTrackLinks()){
            sBuf.append("<track_links>true</track_links>");
        }
        else {
            sBuf.append("<track_links>false</track_links>");
        }
        sBuf.append("<send_date>").append(this.getSendDate()).append("</send_date>");
        if (this.getSendDate().equalsIgnoreCase("immediate")){
            sBuf.append("<send_time></send_time>");
        }
        else {
            sBuf.append("<send_time>").append(this.getSendTime()).append("</send_time>");
        }
        sBuf.append("<lists>");
        
        for (int i =0; i < lists.size(); i++) {
            String listid = (String)lists.get(i);
            sBuf.append("<list>").append(listid).append("</list>");
        }
        sBuf.append("</lists>");
        
		return sBuf.toString();
	}

	/**
	 * @return
	 */
	public String getAdditional() {
		return additional;
	}

	/**
	 * @return
	 */
	public String getFromEmail() {
		return fromEmail;
	}

	/**
	 * @return
	 */
	public String getFromName() {
		return fromName;
	}

	/**
	 * @return
	 */
	public boolean isMultiPartMime() {
		return multiPartMime;
	}

	/**
	 * @return
	 */
	public String getSendDate() {
		return sendDate;
	}

	/**
	 * @return
	 */
	public String getSendTime() {
		return sendTime;
	}

	/**
	 * @return
	 */
	public boolean isTrackLinks() {
		return trackLinks;
	}

	/**
	 * @param string
	 */
	public void setAdditional(String string) {
		additional = string;
	}

	/**
	 * @param string
	 */
	public void setFromEmail(String string) {
		fromEmail = string;
	}

	/**
	 * @param string
	 */
	public void setFromName(String string) {
		fromName = string;
	}

	/**
	 * @param b
	 */
	public void setMultiPartMime(boolean b) {
		multiPartMime = b;
	}

    /**
     * Sends the email immediatly (default)
     *
     */
    public void setSendImmediate() {
        this.sendDate = "immediate";
        this.sendTime = "";
    }
    
    /**
     * 
     * @param month  0-11
     * @param day    1-31
     * @param year   2004 - 9999
     * @throws ExactTargetException
     */
	public void setSendDate(int month, int day, int year) throws ExactTargetException {
        
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        
        Calendar today = Calendar.getInstance();
        if (today.after(cal)) {
            throw new ExactTargetException("Send date must be of the format month=0-11, day = 1-31, year = 2004-9999 and cannot be older than today. Recommend using java.util.Calendar class to set values.");
        }

        try {        
            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            sendDate = df.format(cal.getTime());
        }
        catch (Exception e) {
            throw new ExactTargetException("Send date must be of the format month=0-11, day = 1-31, year = 2004-9999 and cannot be older than today. Recommend using java.util.Calendar class to set values.");        
        }
	}

	/**
     * This is used to schedule the date and time of the email. 
     * Correct format is hh:mm and it uses a 24 hour clock.
     * 
	 * @param string
	 */
	public void setSendTime(String string) throws ExactTargetException {
        if (string == null || string.length() != 5) {
            throw new ExactTargetException("Send time must be of the format hh:mm using a 24 hour clock. Sample: 14:30 for 2:30pm, 02:30 for 2:30am. You provided: " + string);
        }
        
        for (int i=0; i<string.length(); i++) {
            switch (i) {
				case 0 :
                case 1 :
                case 3 :
                case 4 :
                    if (!Character.isDigit(string.charAt(i))) {
                        throw new ExactTargetException("Send time must be of the format hh:mm using a 24 hour clock. Sample: 14:30 for 2:30pm, 02:30 for 2:30am. You provided: " + string);
                    }
					break;
                case 2 :
                if (string.charAt(i) != ':') {
                    throw new ExactTargetException("Send time must be of the format hh:mm using a 24 hour clock. Sample: 14:30 for 2:30pm, 02:30 for 2:30am. You provided: " + string);
                }                
				default :
					break;
			}
        }
        
        String temp = new String(string.substring(0,2));
        int intVal = Integer.parseInt(temp);
        if ( intVal < 0 || intVal > 23 ) {
            throw new ExactTargetException("Send time must be of the format hh:mm using a 24 hour clock. Sample: 14:30 for 2:30pm, 02:30 for 2:30am. You provided: " + string);
        }

        temp = new String(string.substring(3,2));
        intVal = Integer.parseInt(temp);
        if ( intVal < 0 || intVal > 59 ) {
            throw new ExactTargetException("Send time must be of the format hh:mm using a 24 hour clock. Sample: 14:30 for 2:30pm, 02:30 for 2:30am. You provided: " + string);
        }

		sendTime = string;
	}

	/**
	 * @param b
	 */
	public void setTrackLinks(boolean b) {
		trackLinks = b;
	}

    /**
     * Add the specified list id to the recipient list collection
     * @param listid
     */
    public void addList(String listid) {
        if (listid != null) {
            if (!this.lists.contains(listid)){
                this.lists.add(listid);
            }
        }
    }
    
    /**
     * Remove the specified list from the recipient list collection
     * @param listid
     */
    public void removeList(String listid){
        if (listid != null){
            this.lists.remove(listid);        
        }
    }

    /**
     * 
     *
     */    
    private void clearLists() {
        this.lists.clear();
    }
}
