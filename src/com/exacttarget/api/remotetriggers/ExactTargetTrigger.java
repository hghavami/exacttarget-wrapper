/*
 * Created on Dec 13, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.exacttarget.api.remotetriggers;

import com.exacttarget.api.ExactTargetBaseRequest;
import com.exacttarget.api.ExactTargetResponse;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public abstract class ExactTargetTrigger extends ExactTargetBaseRequest {

    protected String emailTemplateID = "";
    
    protected abstract String getActionPart();
    protected abstract String getSearchPart();
    protected abstract String getRequiredValuePart();

	/**
	 * 
	 */
	public ExactTargetTrigger() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.ExactTargetBaseRequest#getSystemSpecificPart()
	 */
	protected final String getSystemSpecificPart() {
        StringBuffer sBuf = new StringBuffer();
        
        sBuf.append("<system>");
        sBuf.append("<system_name>job</system_name>");
        sBuf.append(this.getActionPart());
        sBuf.append(this.getSearchPart());
        sBuf.append(this.getRequiredValuePart());
		sBuf.append("</system>");
        
		return sBuf.toString();
	}

	/**
		 * @return
		 */
	public final String getEmailTemplateID() {
		return emailTemplateID;
	}
	/**
		 * @param string
		 */
	public void setEmailTemplateID(String string) {
		emailTemplateID = string;
	}
    
	protected ExactTargetResponse parseResponse(String rawResponse) {
	    ExactTargetResponse response = null;
	    ExactTargetTriggerResponse successResponse = new ExactTargetTriggerResponse();
	    
	    response = ExactTargetResponse.generateResponse(rawResponse, successResponse);
	             
		return response;
	}

}
