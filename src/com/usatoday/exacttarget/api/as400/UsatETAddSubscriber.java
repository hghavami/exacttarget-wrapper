/*
 * Created on Dec 13, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.exacttarget.api.as400;

import com.exacttarget.api.subscribermanagement.AddSubscriber;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class UsatETAddSubscriber extends AddSubscriber {

    // specific USAT Attributes beyond the ET required attributes
    private String lastName = "";
    private String firstName = "";
    private String accountNum = "";
    private String password = "";
    private String publication = "";
    private String numCopies = "";
    private String subtotal = "";
    private String tax = "";
    private String total = "";
    private String ccLast4Digits = "";
    private String firmName = "";
    private String address1 = "";
    private String address2 = "";
    private String address3 = "";
    private String city = "";
    private String state = "";
    private String zip = "";
    private String homePhone = "";
    private String workPhone = "";

	/**
	 * 
	 */
	public UsatETAddSubscriber() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.usatoday.exacttarget.api.AddSubscriber#getOptionalValues()
	 */
	public String getOptionalValues() {
        StringBuffer sBuf = new StringBuffer();
        // our required values
        sBuf.append("<First__Name>").append(this.getFirstName()).append("</First__Name>");
        sBuf.append("<Last__Name>").append(this.getLastName()).append("</Last__Name>");
        sBuf.append("<Firm__Name>").append(this.getFirmName()).append("</Firm__Name>");
        sBuf.append("<Account__Number>").append(this.getAccountNum()).append("</Account__Number>");
        sBuf.append("<address1>").append(this.getAddress1()).append("</address1>");
        sBuf.append("<address2>").append(this.getAddress2()).append("</address2>");
        sBuf.append("<address3>").append(this.getAddress3()).append("</address3>");
        sBuf.append("<city>").append(this.getCity()).append("</city>");
        sBuf.append("<state>").append(this.getState()).append("</state>");
        sBuf.append("<zip>").append(this.getZip()).append("</zip>");
        sBuf.append("<CC>").append(this.getCcLast4Digits()).append("</CC>");
        sBuf.append("<Password>").append(this.getPassword()).append("</Password>");
        sBuf.append("<Publication>").append(this.getPublication()).append("</Publication>");
        sBuf.append("<Number__Copies>").append(this.getNumCopies()).append("</Number__Copies>");
        sBuf.append("<SubTotal>").append(this.getSubtotal()).append("</SubTotal>");
        sBuf.append("<Tax>").append(this.getTax()).append("</Tax>");
        sBuf.append("<Home__Phone>").append(this.getHomePhone()).append("</Home__Phone>");
        sBuf.append("<Work__Phone>").append(this.getWorkPhone()).append("</Work__Phone>");
        
        return sBuf.toString();
	}

	/**
	 * @return
	 */
	public String getAccountNum() {
		return accountNum;
	}

	/**
	 * @return
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @return
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @return
	 */
	public String getAddress3() {
		return address3;
	}

	/**
	 * @return
	 */
	public String getCcLast4Digits() {
		return ccLast4Digits;
	}

	/**
	 * @return
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return
	 */
	public String getFirmName() {
		return firmName;
	}

	/**
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return
	 */
	public String getHomePhone() {
		return homePhone;
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return
	 */
	public String getNumCopies() {
		return numCopies;
	}

	/**
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return
	 */
	public String getPublication() {
		return publication;
	}

	/**
	 * @return
	 */
	public String getState() {
		return state;
	}

	/**
	 * @return
	 */
	public String getSubtotal() {
		return subtotal;
	}

	/**
	 * @return
	 */
	public String getTax() {
		return tax;
	}

	/**
	 * @return
	 */
	public String getTotal() {
		return total;
	}

	/**
	 * @return
	 */
	public String getWorkPhone() {
		return workPhone;
	}

	/**
	 * @return
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param string
	 */
	public void setAccountNum(String string) {
		accountNum = string;
	}

	/**
	 * @param string
	 */
	public void setAddress1(String string) {
		address1 = string;
	}

	/**
	 * @param string
	 */
	public void setAddress2(String string) {
		address2 = string;
	}

	/**
	 * @param string
	 */
	public void setAddress3(String string) {
		address3 = string;
	}

	/**
	 * @param string
	 */
	public void setCcLast4Digits(String string) {
		ccLast4Digits = string;
	}

	/**
	 * @param string
	 */
	public void setCity(String string) {
		city = string;
	}

	/**
	 * @param string
	 */
	public void setFirmName(String string) {
		firmName = string;
	}

	/**
	 * @param string
	 */
	public void setFirstName(String string) {
		firstName = string;
	}

	/**
	 * @param string
	 */
	public void setHomePhone(String string) {
		homePhone = string;
	}

	/**
	 * @param string
	 */
	public void setLastName(String string) {
		lastName = string;
	}

	/**
	 * @param string
	 */
	public void setNumCopies(String string) {
		numCopies = string;
	}

	/**
	 * @param string
	 */
	public void setPassword(String string) {
		password = string;
	}

	/**
	 * @param string
	 */
	public void setPublication(String string) {
		publication = string;
	}

	/**
	 * @param string
	 */
	public void setState(String string) {
		state = string;
	}

	/**
	 * @param string
	 */
	public void setSubtotal(String string) {
		subtotal = string;
	}

	/**
	 * @param string
	 */
	public void setTax(String string) {
		tax = string;
	}

	/**
	 * @param string
	 */
	public void setTotal(String string) {
		total = string;
	}

	/**
	 * @param string
	 */
	public void setWorkPhone(String string) {
		workPhone = string;
	}

	/**
	 * @param string
	 */
	public void setZip(String string) {
		zip = string;
	}

}
